export default class AudioManager {

    private static _music: Map<cc.AudioClip, number> = new Map();

    private static _effect: Map<number, cc.AudioClip> = new Map();

    private static _musicVolume: number = 1.0;

    /**
     * 音乐音量
     */
    public static get musicVolume() { return AudioManager._musicVolume; }

    private static _effectVolume: number = 1.0;

    /**
     * 特效音量
     */
    public static get effectVolume() { return AudioManager._effectVolume; }

    /**
     * 设置音量（音乐与特效）
     * @param value 音量值（0.0 ~ 1.0）
     */
    public static setVolume(value: number) {
        this.setMusicVolume(value);
        this.setEffectVolume(value);
    }

    /**
     * 设置音乐音量
     * @param value 音量值（0.0 ~ 1.0）
     */
    public static setMusicVolume(value: number) {
        if (value < 0) value = 0.0;
        else if (value > 1) value = 1.0;
        this._musicVolume = value;
        this._music.forEach(id => cc.audioEngine.setVolume(id, this._musicVolume));
    }

    /**
     * 设置特效音量
     * @param value 音量值（0.0 ~ 1.0）
     */
    public static setEffectVolume(value: number) {
        if (value < 0) value = 0.0;
        else if (value > 1) value = 1.0;
        this._effectVolume = value;
        this._effect.forEach((clip, id) => cc.audioEngine.setVolume(id, this._effectVolume));
    }

    /**
     * 播放音频
     * @param clip 音频
     * @param loop 是否循环播放
     */
    public static play(clip: cc.AudioClip, loop: boolean) {
        if (loop) this.playMusic(clip);
        else this.playEffect(clip);
    }

    /**
     * 播放音乐
     * @param clip 音频
     */
    public static playMusic(clip: cc.AudioClip) {
        if (this._music.has(clip)) this.stopMusic(clip);
        let id = cc.audioEngine.play(clip, true, this._musicVolume);
        this._music.set(clip, id);
    }

    /**
     * 停止音乐
     * @param clip 音频
     */
    public static stopMusic(clip: cc.AudioClip) {
        cc.audioEngine.stop(this._music.get(clip));
        this._music.delete(clip);
    }

    /**
     * 停止所有音乐
     */
    public static stopAllMusic() {
        this._music.forEach((id, clip) => this.stopMusic(clip));
    }

    /**
     * 暂停音乐
     * @param clip 音频
     */
    public static pauseMusic(clip: cc.AudioClip) {
        cc.audioEngine.pause(this._music.get(clip));
    }

    /**
     * 暂停所有音乐
     */
    public static pauseAllMusic() {
        this._music.forEach((id, clip) => this.pauseMusic(clip));
    }

    /**
     * 恢复音乐
     * @param clip 音频
     */
    public static resumeMusic(clip: cc.AudioClip) {
        cc.audioEngine.resume(this._music.get(clip));
    }

    /**
     * 恢复所有音乐
     */
    public static resumeAllMusic() {
        this._music.forEach((id, clip) => this.resumeMusic(clip));
    }

    /**
     * 播放特效音频
     * @param clip 音频
     */
    public static playEffect(clip: cc.AudioClip) {
        let id = cc.audioEngine.play(clip, false, this._effectVolume);
        this._effect.set(id, clip);
        cc.audioEngine.setFinishCallback(id, () => this._effect.delete(id));
    }

    /**
     * 停止特效音频
     * @param clip 音频
     */
    public static stopEffect(clip: cc.AudioClip) {
        this._effect.forEach((_clip, id) => {
            if (_clip === clip) {
                cc.audioEngine.stop(id);
                this._effect.delete(id);
            }
        });
    }

    /**
     * 停止所有特效音频
     */
    public static stopAllEffect() {
        this._effect.forEach((clip, id) => {
            cc.audioEngine.stop(id);
            this._effect.delete(id);
        });
    }

    /**
     * 暂停特效音频
     * @param clip 音频
     */
    public static pauseEffect(clip: cc.AudioClip) {
        this._effect.forEach((_clip, id) => _clip === clip && cc.audioEngine.pause(id));
    }

    /**
     * 暂停所有特效音频
     */
    public static pauseAllEffect() {
        this._effect.forEach((clip, id) => cc.audioEngine.pause(id));
    }

    /**
     * 恢复特效音频
     * @param clip 音频
     */
    public static resumeEffect(clip: cc.AudioClip) {
        this._effect.forEach((_clip, id) => _clip === clip && cc.audioEngine.resume(id));
    }

    /**
     * 恢复所有特效音频
     */
    public static resumeAllEffect() {
        this._effect.forEach((clip, id) => cc.audioEngine.resume(id));
    }

    /**
     * 停止所有音频
     */
    public static stopAll() {
        this.stopAllMusic();
        this.stopAllEffect();
    }

    /**
     * 暂停所有音频
     */
    public static pauseAll() {
        this.pauseAllMusic();
        this.pauseAllEffect();
    }

    /**
     * 恢复所有音频
     */
    public static resumeAll() {
        this.resumeAllMusic();
        this.resumeAllEffect();
    }
}
